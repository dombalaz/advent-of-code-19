package code.of.advent

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestDay01 {
    @Test
    fun computeFuelForMass() {
        assertEquals(2, fuelForMass(12))
        assertEquals(2, fuelForMass(14))
        assertEquals(654, fuelForMass(1969))
        assertEquals(33583, fuelForMass(100756))
    }

    @Test
    fun computeTotalFuelForMass() {
        assertEquals(2, totalFuelForMass(14))
        assertEquals(966, totalFuelForMass(1969))
        assertEquals(50346, totalFuelForMass(100756))
    }
}