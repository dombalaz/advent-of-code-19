package code.of.advent

import org.junit.jupiter.api.Assertions.assertArrayEquals
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestDay02 {
    @Test
    fun computeIntProgram() {
        assertArrayEquals(arrayOf(3500, 9, 10, 70, 2, 3, 11, 0, 99, 30, 40, 50),
            evalIntCode(arrayOf(1, 9, 10, 3, 2, 3, 11, 0, 99, 30, 40, 50)))
        assertArrayEquals(arrayOf(2, 0, 0, 0, 99),
            evalIntCode(arrayOf(1, 0, 0, 0, 99)))
        assertArrayEquals(arrayOf(2, 3, 0, 6, 99),
            evalIntCode(arrayOf(2, 3, 0, 3, 99)))
        assertArrayEquals(arrayOf(2, 4, 4, 5, 99, 9801),
            evalIntCode(arrayOf(2, 4, 4, 5, 99, 0)))
        assertArrayEquals(arrayOf(30, 1, 1, 4, 2, 5, 6, 0, 99),
            evalIntCode(arrayOf(1, 1, 1, 4, 99, 5, 6, 0, 99)))
    }
}