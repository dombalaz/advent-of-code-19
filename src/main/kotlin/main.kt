package code.of.advent

import java.io.File

fun getResourceAsText(path: String): String {
    return object {}.javaClass.getResource(path).readText()
}

fun main(args: Array<String>) {
    if (args.size != 1) {
        println("Enter one number as argument. The number should be between 1 and 25 inclusive.")
        return
    }
    val computeDay = args[0].toInt()
    val input = File("src/main/resources/day${if (computeDay < 10) "0" else ""}$computeDay.txt").readLines()
    when (computeDay) {
        1 -> computeDay01(input)
        2 -> computeDay02(input)
        else -> throw IllegalArgumentException("The number should be between 1 and 25 inclusive")
    }
}