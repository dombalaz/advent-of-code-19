package code.of.advent

fun fuelForMass(mass: Int) = if (mass >= 6) mass / 3 - 2 else 0

fun totalFuelForMass(mass: Int): Int {
    var sum = 0
    var fuelMass = mass
    do {
        fuelMass = fuelForMass(fuelMass)
        sum += fuelMass
    } while (fuelMass > 0)
    return sum
}

fun computeDay01(input: List<String>) {
    var sum = 0
    var sum1 = 0
    for (it in input) {
        sum += fuelForMass(it.toInt())
        sum1 += totalFuelForMass(it.toInt())
    }
    println("Day01 - task 1: $sum - task 2: $sum1")
}