package code.of.advent

fun evalIntCode(program: Array<Int>) : Array<Int> {
    loop@ for (i in program.indices step 4) {
        when (program[i]) {
            1 -> program[program[i + 3]] = program[program[i + 1]] + program[program[i + 2]]
            2 -> program[program[i + 3]] = program[program[i + 1]] * program[program[i + 2]]
            99 -> break@loop
            else -> {
                println("Invalid instruction.")
                return program
            }
        }
    }
    return program
}

fun computeDay02(program: List<String>) {
    val intCode = program[0].split(",").map { it.toInt() }

    val modifiedIntCode = intCode as MutableList
    modifiedIntCode[1] = 12
    modifiedIntCode[2] = 2
    val result = evalIntCode(modifiedIntCode.toTypedArray())

    loop@ for (i in 0..99) {
        for (j in 0..99) {
            intCode[1] = i
            intCode[2] = j
            val tmpResult = evalIntCode(intCode.toTypedArray())
            if(tmpResult[0] == 19690720) {
                println("Day 2 - task 2: ${100 * i + j}")
                break@loop
            }
        }
    }

    println("Day 2 - task 1: ${result[0]}")
}