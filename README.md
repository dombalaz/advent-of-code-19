# Advent of Code 2019
This project contains solutoins for [Advent of Code 2019](http://adventofcode.com/2019) written in [Kotlin](https://kotlinlang.org/). I encourage you to try to solve problems alone before looking at my imperfect code.

## Currently solved problems
- Day 01